package toan.nihongo;

import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StartAppSDK.init(this, "210223470", true);
		StartAppAd.showSplash(this, savedInstanceState);
		Log.e("MainActivity", "onCreate");
		setContentView(R.layout.activity_main);
		
		getSupportActionBar().setTitle("Nihongo");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onRomaji(View view) {
		Intent intent = new Intent(this, SecondActivity.class);
		intent.putExtra("type", SecondActivity.TYPE_ROMAJI);
		startActivity(intent);
	}
	
	public void onHiragana(View view) {
		Intent intent = new Intent(this, SecondActivity.class);
		intent.putExtra("type", SecondActivity.TYPE_HIRAGANA);
		startActivity(intent);
	}
	
	public void onKatakana(View view) {
		Intent intent = new Intent(this, SecondActivity.class);
		intent.putExtra("type", SecondActivity.TYPE_KATAKANA);
		startActivity(intent);
	}
}
