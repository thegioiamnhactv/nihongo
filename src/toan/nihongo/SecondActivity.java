package toan.nihongo;

import java.util.Random;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends ActionBarActivity {
	
	public static final int TYPE_ROMAJI = 0;
	public static final int TYPE_HIRAGANA = 1;
	public static final int TYPE_KATAKANA = 2;
	
	private int mType;
	private Random rand;
	
	public static final String[] romaji = new String[] {
		"a", "i", "u", "e", "o",
		"ka", "ki", "ku", "ke", "ko",
		"sa", "shi", "su", "se", "so",
		"ta", "chi", "tsu", "te", "to",
		"na", "ni", "nu", "ne", "no",
		"ha", "hi", "fu", "he", "ho",
		"ma", "mi", "mu", "me", "mo",
		"ya", "yu", "yo",
		"ra", "ri", "ru", "re", "ro",
		"wa", "wo",
		"n"
	};
	
	public static final String[] hiragana = new String[] {
		"あ", "い", "う", "え", "お",
		"か", "き", "く", "け", "こ",
		"さ", "し", "す", "せ", "そ",
		"た", "ち", "つ", "て", "と",
		"な", "に", "ぬ", "ね", "の",
		"は", "ひ", "ふ", "へ", "ほ",
		"ま", "み", "む", "め", "も",
		"や", "ゆ", "よ",
		"ら", "り", "る", "れ", "ろ",
		"わ", "を",
		"ん"
	};
	
	public static final String[] katakana = new String[] {
		"ア", "イ", "ウ", "エ", "オ",
		"カ", "キ", "ク", "ケ", "コ",
		"サ", "シ", "ス", "セ", "ソ",
		"タ", "チ", "ツ", "テ", "ト",
		"ナ", "ニ", "ヌ", "ネ", "ノ",
		"ハ", "ヒ", "フ", "ヘ", "ホ",
		"マ", "ミ", "ム", "メ", "モ",
		"ヤ", "ユ", "ヨ",
		"ラ", "リ", "ル", "レ", "ロ",
		"ワ", "ヲ",
		"ン"
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		Intent intent = getIntent();
		mType = intent.getIntExtra("type", TYPE_ROMAJI);
		
		String title = mType == TYPE_ROMAJI ? "Romaji" : mType == TYPE_HIRAGANA ? "Hiragana" : "Katakana";
		getSupportActionBar().setTitle(title);
		
		rand = new Random();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.second, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onClick(View view) {
		switch (mType) {
		case TYPE_ROMAJI:
			((TextView) findViewById(R.id.textView1)).setText(romaji[rand.nextInt(romaji.length)]);
			break;
			
		case TYPE_HIRAGANA:
			((TextView) findViewById(R.id.textView1)).setText(hiragana[rand.nextInt(hiragana.length)]);
			break;
			
		case TYPE_KATAKANA:
			((TextView) findViewById(R.id.textView1)).setText(katakana[rand.nextInt(katakana.length)]);
			break;

		default:
			break;
		}
	}
}
